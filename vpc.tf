module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "terraform-vpc"
  cidr = "10.50.0.0/16"

  azs             = ["us-east-2a", "us-east-2b", "us-east-2c"]
  private_subnets = ["10.50.1.0/24"]
  public_subnets  = ["10.50.10.0/24","10.50.20.0/24"]

  enable_nat_gateway = true
  enable_vpn_gateway = true

  tags = {
    "Cost Center" = "Cloud Operations"
    Terraform = "true"
    Environment = "presales"
  }
}

output "vpc-cidr" {
  value = module.vpc.vpc_cidr_block
}
output "Public-Subnets" {
  value = module.vpc.public_subnets
}
output "Private-Subnets" {
  value = module.vpc.private_subnets
}
output "Availability-Zones" {
  value = module.vpc.azs
}